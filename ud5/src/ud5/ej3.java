package ud5;

import javax.swing.JOptionPane;

public class ej3 {

	public static void main(String[] args) {
		
		/*
			3) Modifica la aplicación anterior, para que nos pida el nombre que queremos introducir	(recuerda usar JOptionPane).
		 */
		
		// declarar variables

			String nombre=JOptionPane.showInputDialog("Introduce tu nombre");
			
			JOptionPane.showMessageDialog(null, "Bienvenido " + nombre);
	}

}
