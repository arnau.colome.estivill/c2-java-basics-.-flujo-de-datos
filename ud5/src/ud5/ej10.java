package ud5;

import java.util.Scanner;

public class ej10 {

	public static void main(String[] args) {
	
		/*
			10) Realiza una aplicaci�n que nos pida un n�mero de ventas a introducir, despu�s nos
			pedir� tantas ventas por teclado como n�mero de ventas se hayan indicado. Al final
			mostrara la suma de todas las ventas. Piensa que es lo que se repite y lo que no.
		*/

		Scanner teclat = new Scanner(System.in);
		
		System.out.println("Numero de ventas a introducir");
		int numero = teclat.nextInt();
		double resultado=0;
		
		for (int i = 1; i <= numero; i++) {
			System.out.println("Precio del producto");
			double precio = teclat.nextDouble();
			resultado = resultado + precio;
		}
		
		System.out.println("Total: " + resultado + "�");
		
		teclat.close();
		
	}

}
