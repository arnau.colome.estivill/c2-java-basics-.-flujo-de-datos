package ud5;

import java.util.Scanner;

public class ej13 {

	public static void main(String[] args) {

		/*
			13) Crea una aplicaci�n llamada CalculadoraInversa, nos pedir� 2 operandos (int) y un signo
			aritm�tico (String), seg�n este �ltimo se realizara la operaci�n correspondiente. Al final
			mostrara el resultado en un cuadro de dialogo.
			Los signos aritm�ticos disponibles son:
				+: suma los dos operandos.
				-: resta los operandos.
				*: multiplica los operandos.
				/: divide los operandos, este debe dar un resultado con decimales (double)
				^: 1o operando como base y 2o como exponente.
				%: m�dulo, resto de la divisi�n entre operando1 y operando2.
		*/
		
		Scanner teclat = new Scanner(System.in);
		
		// Variables
		
		int num1 = 0,  num2 = 0;
		String operacion;

		// Operaciones
		
		System.out.println("Numero 1: ");
		num1 = teclat.nextInt();
		
		System.out.println("Numero 2: ");
		num2 = teclat.nextInt();
		
		System.out.println("Operacion (+, -, *, ^, %): ");
		operacion = teclat.next();
		
		if (operacion.contentEquals("+")) {
			System.out.println("Resultado suma: " + (num1 + num2));
		}
		else if(operacion.contentEquals("-")) {
			System.out.println("Resultado resta: " + (num1 - num2));
		}
		else if (operacion.contentEquals("*")) {
			System.out.println("Resultado multiplicaci�n: " + (num1 * num2));
		}
		else if (operacion.contentEquals("/")) {
			System.out.println("Resultador divisi�n: " + (num1 / num2));
		}
		else if (operacion.contentEquals("^")) {
			System.out.println("Resultado num1 operando como base y num2 como exponente: " + (Math.pow(num1, num2)));
		}
		else if (operacion.contentEquals("%")) {
			System.out.println("Resultado modulo: " + (num1 % num2));
		}
		else {
			System.out.println("operador no valido");
		}
		teclat.close();
	}

}
