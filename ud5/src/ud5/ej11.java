package ud5;

import java.util.Scanner;

public class ej11 {

	public static void main(String[] args) {
		
		//	Crea una aplicaci�n que nos pida un d�a de la semana y que nos diga si es un d�a laboral o no. Usa un switch para ello.
		
		Scanner teclat = new Scanner(System.in);
		
		System.out.println("Que dia de la semana?");
		String dia = teclat.next();
		
		switch (dia) {
			case "Lunes":
				System.out.println("Dia laboral");
				break;
			case "Martes":
				System.out.println("Dia laboral");
				break;
			case "Miercoles":
				System.out.println("Dia laboral");
				break;
			case "Jueves":
				System.out.println("Dia laboral");
				break;
			case "Viernes":
				System.out.println("Dia laboral");
				break;
			case "Sabado":
				System.out.println("Dia no laboral");
				break;
			case "Domingo":
				System.out.println("Dia no laboral");
				break;
			default:
				System.out.println("dia err�neo");
		}
		
		teclat.close();
		
	}

}
