package ud5;

import java.util.Scanner;

public class ej4 {

	public static void main(String[] args) {
		/*
		 	4) Haz una aplicaci�n que calcule el �rea de un circulo (pi*R2). 
		 	El radio se pedir� por teclado
			(recuerda pasar de String a double con Double.parseDouble). Usa la constante PI y el m�todo pow de Math.
		*/
		
		Scanner teclat = new Scanner(System.in);
		
		double R=0;
		double b=2;
		
		System.out.println("Radio del circulo:");
		R = teclat.nextDouble();
		
		double resultado = Math.PI * (Math.pow(R, b));
		
		System.out.println("Area del circulo: " + resultado);
		teclat.close();
		
	}

}
