package ud5;

public class ej9 {

	public static void main(String[] args) {
		
		// 9) Muestra los n�meros del 1 al 100 (ambos incluidos) divisibles entre 2 y 3. Utiliza el bucle que desees.

		int i = 1;
		
		while (i <= 100) {
			
			if (i%2 == 0 || i%3 == 0) {
				System.out.println(i);
			}
			
			i++;
		}
		
	}

}
