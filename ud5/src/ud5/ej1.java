package ud5;

public class ej1 {

	public static void main(String[] args) {
		
		/*
			1) Declara 2 variables num�ricas (con el valor que desees), he indica cual es mayor de los
			dos. Si son iguales indicarlo tambi�n. Ves cambiando los valores para comprobar que	funciona.
		 */
		
		// declarar variables

			int A = 10;
			
			int B = 6;
			
			//funcion con if para ver que variable es mas grande o si son iguales
			
			if (A > B) {
				System.out.println("La variable A es m�s grande con un valor de: " + A);
			}
			else if (A == B) {
				System.out.println("Las dos variables tienen el mismo valor con: " + A);
			}
			else {
				System.out.println("La variable B es m�s grande con un valor de: " + B);
			}
		
	}

}
